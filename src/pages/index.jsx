import React, { useRef } from "react";
import Navbar from "../components/navbar";
import Svg1 from "../components/svg/svg1.svg";
import Svg2 from "../components/svg/svg2.svg";
import About from "../components/about";
import { ReactSVG } from "react-svg";
import Causes from "../components/causes";
import WhoWeAre from "../components/whoweare";
import Why from "../components/why";
import How from "../components/how";
import Services from "../components/services";
import Gallery from "../components/gallery";
import Volunteer from "../components/volunteer";
import Stories from "../components/stories";
import Footer from "../components/footer";
import { Typewriter } from "react-simple-typewriter";
import { Fade, Slide } from "react-reveal";

function Index() {
  const text = [
    "Nurturing Tommorrow's Tech Talent",
    "The future of the country lies in the hands of the young",
    "~~ President Paul Kagame",
  ];
  const about = useRef();
  const causes = useRef();
  const services = useRef();
  const testimonies = useRef();

  const scrollToAbout = () => {
    console.log("Scroll to about!");
    about.current?.scrollIntoView({ behavior: "smooth" });
  };
  const scrollToCauses = () => {
    console.log("causes");
    causes.current?.scrollIntoView({ behavior: "smooth" });
  };
  const scrollToServices = () => {
    services.current?.scrollIntoView({ behavior: "smooth" });
  };
  const scrollToTestimonies = () => {
    testimonies.current?.scrollIntoView({ behavior: "smooth" });
  };
  const scrollObj = {
    about: scrollToAbout,
    causes: scrollToCauses,
    services: scrollToServices,
    testimonies: scrollToTestimonies,
  };

  return (
    <div className="h-fit w-full">
      <div className="w-full bg-bmain1 sm:h-[90vh] lg:h-[100vh]">
        <Navbar scrollObj={scrollObj} />
        <img alt="" src={Svg1} />
        <div className="sm:mt mt-[5%] flex h-[80vh]  w-full pl-[5%] sm:h-[70vh] lg:mt-[2%] lg:h-[80vh]">
          <div className="px:[50%] h-full w-[50%]">
            <div className="flex h-full flex-col items-start gap-2">
              <p className="leading-snugsm:text-4xl flex h-[30%]  w-full items-center  font-rowdie text-main2 lg:text-5xl xl:text-6xl">
                <Typewriter
                  words={[text[0], text[1], text[2]]}
                  loop={true}
                  typeSpeed={120}
                />
              </p>
              <p className="w-full font-manrope text-2xl leading-snug text-main3">
                It is about giving back to the community, and making the world a
                better place.
              </p>
              <button className="background-animate h-12 rounded-3xl bg-gradient-to-r from-main1 via-pink to-main1 px-[5%] font-manrope font-extrabold text-white focus:outline-none">
                CONTINUE
              </button>
            </div>
          </div>

          <div className="relative flex w-[50%] justify-end sm:items-center">
            <Fade>
            <img
              alt=""
              src="../img1.jpg"
              className="object-fit absolute rounded-full sm:h-[40%] lg:right-10 lg:h-[60vh] lg:w-[25vw] lg:w-[32vw]  xl:right-20 xl:h-[50vh] xl:w-[32vw]"
            />
            <img
              src="../img2.jpg"
              className="object-fit absolute h-48 w-48 rounded-full sm:top-40  xl:right-10"
              alt=""
            />
            </Fade>
            <div className="hidden lg:hidden xl:block">
              <ReactSVG src={Svg2} />
            </div>
          </div>
        </div>
        <Fade top>
          <div className="z-50 mt-[-12%] flex items-center justify-center rounded-lg bg-white font-rowdie shadow-lg shadow-slate-200 sm:mx-[10%] sm:w-[80%] lg:mx-[25%] lg:mt-[-6%] lg:h-36 lg:w-[60%] xl:w-[55%]">
            <div className="flex gap-12">
              <div className="flex flex-col items-center gap-4">
                <p className="h-12 w-12 rounded-sm border-b-2 border-black text-center text-3xl font-bold text-pink">
                  1
                </p>
                <p className="text-center">Number of Supporters</p>
              </div>
              <div className="flex flex-col items-center gap-4">
                <p className="h-12 w-12 rounded-sm border-b-2 border-black text-center text-3xl font-bold text-pink">
                  5
                </p>
                <p className="text-center">Number of Volunteers</p>
              </div>
              <div className="flex flex-col items-center gap-4">
                <p className="h-12 w-12 rounded-sm border-b-2 border-black text-center text-3xl font-bold text-pink">
                  15
                </p>
                <p className="text-center">We've helped Raise</p>
              </div>
            </div>
          </div>
        </Fade>
      </div>
      <About ref={about} />
      <Causes ref={causes} />
      <WhoWeAre />
      <Why />
      <How />
      <Services />
      <Gallery />
      <Volunteer />
      <Stories />
      <Footer />
    </div>
  );
}

export default Index;
