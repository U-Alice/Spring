import { Fade } from "react-reveal";
import Header from "./header";

export default function Causes() {
  const data = [
    {
      title: "EDUCATION",
      img: "../../img5.jpg",
      desc: "Bring Together people who care about a cause!",
    },
    {
      title: "CHARITY",
      img: "../../img4.jpg",
      desc: "Bring Together people who care about a cause!",
    },
    {
      title: "COMMUNITY",
      img: "../../cause3.jpg",
      desc: "Charity requires only a good heart!",
    },
  ];
  return (
    <div className="my-[10%] flex h-fit w-full flex-col items-center justify-center gap-12 font-manrope font-bold">
      <Header title={"our Causes"} subTitle={"What To Expect"} />
      <Fade bottom>
        <div className="flex h-fit w-[100%] flex-wrap justify-center gap-12 sm:w-[90%] md:gap-6 xl:w-[70%]">
          {data.map((item) => {
            return (
              <div className="relative h-fit w-[70%] md:w-[30%] lg:w-[30%]">
                <p className="absolute left-4 top-4 h-8 w-32 rounded-2xl bg-pink p-1 text-center text-white">
                  {item.title}
                </p>
                <div className="">
                  <img src={item.img} alt="" className="w-full rounded-t-xl" />
                  <p className=" h-20 bg-bmain1 p-2 text-left font-rowdie text-lg font-light text-main2">
                    {item.desc}{" "}
                  </p>
                </div>
              </div>
            );
          })}
        </div>
      </Fade>
      <button className="rounded-3xl border border-main3 p-3 font-manrope font-extrabold text-main3">
        Explore All
      </button>
    </div>
  );
}
