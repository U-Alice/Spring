import { Fade } from "react-reveal";
import Header from "./header";

export default function Volunteer() {
  const content = [
    {
      image: "../../Alice.png",
      testimony: "Hello, My name is, ..",
    },
    {
      image: "../../iris.png",
      testimony: "Hello, My name is, ..",
    },
    {
      image: "../../Axelle.png",
      testimony: "Hello, My name is, ..",
    },
    {
      image: "../../yvan.png",
      testimony: "Hello, My name is, ..",
    },
  ];
  return (
    <div className="h-fit bg-[#F3F2E7] py-12">
      <Header title={"VOLUNTEERS"} subTitle={"Our Volunteers"} />
      <Fade>
        <div className="mt-12 flex w-full flex-wrap justify-center gap-2 sm:gap-4 lg:gap-12 lg:px-32">
          {content.map((item) => {
            return (
              <div className="h-[40vh] w-[45vw] bg-white sm:w-[30vw] lg:w-fit">
                <img src={item.image} className="h-[80%] object-cover" alt="" />
                <p className="text-center">{item.testimony}</p>
              </div>
            );
          })}
        </div>
      </Fade>
    </div>
  );
}
