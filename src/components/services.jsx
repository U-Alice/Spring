import { Fade } from "react-reveal";
import Header from "./header";

export default function Services() {
  const content = [
    {
      title: "Lorem",
      content:
        "This simply dummy text of the typesetting industry. Lorem Ipsum has been the industry's dummy text",
    },
    {
      title: "Ipsum",
      content:
        "This simply dummy text of the typesetting industry. Lorem Ipsum has been the industry's dummy text",
    },
    {
      title: "Lorem Ipsum",
      content:
        "This simply dummy text of the typesetting industry. Lorem Ipsum has been the industry's dummy text",
    },
  ];
  return (
    <div className="w-full ">
      <Header title={"Our Services"} subTitle={"Services We Provide"} />
      <Fade top>
      <div className="mt-[5%] flex flex-wrap w-full justify-center p-4 lg:gap-[5%] sm:gap-[2%]">
        {content.map((item) => {
          return (
            <div className="h-fit p-4 lg:w-[30%] xl:w-[20%] md:w-[45%] sm:w-[60%] w-[80%] my-2 rounded-2xl bg-bmain1 shadow-sm shadow-slate-200">
              <div className="flex items-center gap-4 h-24 text-2xl text-main2">
                <img src="../../../mission.png" alt="" />
                <p className="font-rowdie font-extrabold">{item.title}</p>
              </div>
              <div className="h-[0.5px] w-full bg-neutral-200">
                <div
                  className="h-[3px] bg-main2"
                  style={{ width: "22%" }}
                ></div>
              </div>
              <h3 className="mt-12 font-manrope text-xl text-main3">
                {item.content}
              </h3>
              <button className="font-rowdies mt-12 w-[50%] rounded-3xl bg-main3 p-3 font-extrabold text-white">
                About Us
              </button>
            </div>
          );
        })}
      </div>
      </Fade>
    </div>
  );
}
