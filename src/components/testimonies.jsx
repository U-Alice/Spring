import { useState } from "react";

export default function Testimonies() {
  const [current, setCurrent] = useState(4);
  const content = [{}, {}, {}, {}, {}, {}, {}, {}, {}];

  const updateIndex = (index) => {
    return setCurrent(index);
  };

  const handleKeyDown = (index) => {};
  return (
    <div className="h-fit w-full">
      <div className="flex w-[100%] justify-center">
        {content.map((item, index) => {
          return (
            <div
              className={`h-full w-[5%] border-t ${
                index === current
                  ? "flex-col border-t-2 border-black"
                  : "border-grey flex-row"
              } flex items-center justify-center py-4 transition duration-700 `}
              onClick={() => {
                return updateIndex(index);
              }}
              onKeyDown={handleKeyDown}
              aria-hidden="true"
            >
              <img
                src="../cause3.jpg"
                alt=""
                className={`${
                  index === current ? "h-16 w-16" : "h-10 w-10 "
                } my-2 object-fill rounded-full  transition duration-700`}
              />
              <p
                className={`text-center font-rowdie text-xl font-bold ${
                  index === current ? "" : "hidden"
                }`}
              >
                Alice
              </p>
            </div>
          );
        })}
      </div>
    </div>
  );
}
