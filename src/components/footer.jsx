import { fa0 } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ReactSVG } from "react-svg";
import FacebookSVG from '../components/svg/facebook.svg'
export default function Footer() {
  const contacts = [
    {
      icon: "",
      title: "Phone",
      contact: "+250 785937422",
      bg: "#EAE9DA",
    },
    {
      icon: "",
      title: "Email",
      contact: "umugwanezaalice22@gmail.com",
      bg: "#E3E1CB",
    },
    {
      icon: "",
      title: "Address",
      contact: "Nyabihu,",
      bg: "#CBC8AD",
    },
  ];
  return (
    <div className="w-full md:h-[60vh] ">
      <div className="flex h-[20%] w-full flex-wrap justify-center">
        {contacts.map((item) => {
          return (
            <div
              className={`flex h-full sm:w-1/2 w-full flex-col items-center justify-center p-2 md:w-1/3 md:flex-row bg-[${item.bg}] gap-4`}
            >
              <div className="h-10 w-10 rounded-full bg-white md:h-12 md:w-12"></div>
              <div className="w-[70%] font-manrope">
                <p className="text-center text-[#3A1700] opacity-50 md:text-left">
                  {item.title}
                </p>
                <p className="text-center text-main2 md:text-left">
                  {item.contact}
                </p>
              </div>
            </div>
          );
        })}
      </div>
      <div className="h-[80%] w-full bg-footer px-[8%] py-24 ">
        <div className="h-[85%] w-full font-manrope">
          <div className="flex w-full  flex-col  md:flex-row">
            <div className="w-1/6">
              <p className="font-rowdie text-3xl text-white">Spring</p>
            </div>
            <div className="flex w-full flex-col items-center gap-2 text-lg text-white">
              <p className="font-rowdie font-bold">MENU</p>
              <p>About</p>
              <p>Causes</p>
              <p>Services</p>
              <p>Volunteers</p>
            </div>
            <div className="flex w-full flex-col items-center gap-2 text-lg text-white">
              <p className="font-rowdie font-bold">MENU</p>
              <p>Direct Help</p>
              <p>Giving Information</p>
              <p>Raising Awareness</p>
              <p>Helping the Community</p>
            </div>
            <div className="flex gap-4">
              <div className="flex h-8 w-8 items-center justify-center rounded-full bg-[#3A170080] md:h-14 md:w-14">
                <FontAwesomeIcon
                  icon="fa-brands fa-instagram"
                  className="h-12 w-32 text-white"
                />
              </div>
              <div className="flex h-8 w-8 items-center justify-center rounded-full bg-[#3A170080] md:h-14 md:w-14">
              <ReactSVG icon={FacebookSVG}  />
              </div>
              <div className="flex h-8 w-8 items-center justify-center rounded-full bg-[#3A170080] md:h-14 md:w-14"></div>
            </div>
          </div>
        </div>
        <div className="flex h-[8%] w-full justify-between border-t border-white p-8  font-manrope text-lg text-[#FFFFFF80]">
          <div>Copyright © 2023 Spring, All Rights Reserved!</div>
          <div className="flex gap-8">
            <p>Terms of Use</p>
            <p>Privacy Policy</p>
          </div>
        </div>
      </div>
    </div>
  );
}
