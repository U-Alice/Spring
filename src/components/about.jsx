import { Bounce, Fade } from "react-reveal";
import Header from "./header";

function About() {
  const content = [
    {
      title: "Mission",
      text: "Charities empower people to make a difference, even if it is just in a small way",
    },
    {
      title: "Vision",
      text: "Charities empower people to make a difference, even if it is just in a small way",
    },
    {
      title: "Raising Awareness",
      text: "Charities empower people to make a difference, even if it is just in a small way",
    },
    {
      title: "Community Engagement",
      text: "Charities empower people to make a difference, even if it is just in a small way",
    },
  ];
  return (
    <div className="mt-[15%] h-fit w-full font-rowdie xl:mt-[10%]">
      <Header title={"About us"} subTitle={"What to expect"} />
      <div className="md:my my-[5vh] flex h-full w-full flex-wrap items-center justify-center">
        <div className="flex w-[80%] flex-wrap gap-4 lg:w-[70%]">
          {content.map((item) => {
            return (
              <Bounce>
                <div className="h-[25%] rounded-2xl bg-bmain1 px-[5%] py-[2%] text-justify shadow-sm shadow-slate-200 md:w-[48%]">
                  <div className="flex items-center gap-4 text-2xl text-main2">
                    <img src="../../../mission.png" alt="" />
                    <p>{item.title}</p>
                  </div>
                  <div className="h-[0.5px] w-full bg-neutral-200">
                    <div
                      className="h-[3px] bg-main2"
                      style={{ width: "22%" }}
                    ></div>
                  </div>
                  <h3 className="mt-12 font-manrope text-lg text-main3">
                    {item.text}
                  </h3>
                </div>
              </Bounce>
            );
          })}
        </div>
      </div>
    </div>
  );
}
export default About;
