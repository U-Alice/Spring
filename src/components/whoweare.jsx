import { Fade } from "react-reveal";
import Header from "./header";

export default function WhoWeAre() {
  return (
    <Fade bottom>
    <div className="flex w-full flex-col justify-center items-center font-manrope sm:flex-row xl:px-32">
      <div className="flex relative w-[50%]">
        <img src="../../who.png" alt="" />
        <img src="../../who1.png" alt="" className="absolute right-40"/>
      </div>
      <div className="flex w-[90%] flex-col items-center justify-center gap-12 sm:w-[50%]">
        <div>
          <Header title={"WHO"} subTitle={"Who Are We"} />
        </div>
        <p className="text-xl font-light">
          We are a small group of students who are passionate in our studies
          that revolve aroung software engineering and wanted to give back to
          the community around us.  
        </p>
        <button className="rounded-3xl border border-main3 p-3 font-manrope font-extrabold text-main3 ">
          About Us
        </button>
      </div>
    </div>
    </Fade>
  );
}
