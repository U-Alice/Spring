import { Fade } from "react-reveal";
import Header from "./header";

export default function How() {
  const content = [
    {
      nbr: 1,
      content:
        "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore ",
    },
    {
      nbr: 2,
      content:
        "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore ",
    },
    {
      nbr: 3,
      content:
        "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore ",
    },
  ];
  return (
    <div className="h-fit w-full">
      <Header title={"HOW"} subTitle={"What to expect"} />
      <Fade bottom >
        <div className="flex h-[80vh] items-center justify-center gap-2">
          {content.map((item) => {
            return (
              <div className="h-full w-[30%]">
                <div
                  className={`${
                    item.nbr === 2
                      ? "mt-[40%]"
                      : item.nbr === 3
                        ? "mt-[60%]"
                        : ""
                  }`}
                ></div>
                <div className="h-[30%] ">
                  <div className="flex h-16 w-16 items-center justify-center rounded-full bg-[#F3F2E7] sm:h-36 sm:w-36">
                    <p className="text-3xl font-bold text-[#CE7C0280] sm:text-6xl ">
                      {item.nbr}
                    </p>
                  </div>
                  <p className="text-md font-100 mt-8 font-manrope sm:text-xl">
                    {item.content}
                  </p>
                </div>
              </div>
            );
          })}
        </div>
      </Fade>
    </div>
  );
}
