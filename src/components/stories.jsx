import Header from "./header";
import quote from "./svg/quote.svg"
import Testimonies from "./testimonies";
export default function Stories(){
    return (
        <div className="mt-32">
            <Header title={"CHILDREN'S STORIES"} subTitle={'Read Stories'}/>
            <div className="w-full flex flex-col  items-center py-12">
                <img src={quote} alt="" className="w-12 h-12 my-2"/>
                <p className="font-rowdie text-2xl w-[50%] text-center my-4">
                Quis autem vel eum i[r]ure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariaturu
                </p>
            </div>
            <Testimonies/>
        </div>
    )
}