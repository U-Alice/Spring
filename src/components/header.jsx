function Header({ title, subTitle }) {
  return (
    <div className="w-full h-[10%] md:h-[15%] font-rowdie leading-loose">
      <p className="text-center text-xl font-bold text-pink">{title}</p>
      <h1 className="text-center text-4xl text-main2 ">{subTitle}</h1>
    </div>
  );
}
export default Header;
