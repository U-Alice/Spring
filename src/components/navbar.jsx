import React, { useEffect, useState } from 'react'

function Navbar({scrollObj}) {
    const [clientWindowHeight, setClientWindowHeight] = useState(0);
    const [bg, setBg]= useState("transparent h-36")
    const handleScroll = ()=>{
        setClientWindowHeight(window.scrollY);
    }   
    useEffect(()=>{
        window.addEventListener("scroll", handleScroll);
        return ()=> window.removeEventListener("scroll", handleScroll);
    })
    useEffect(()=>{
        if(clientWindowHeight > 100){
            setBg("white text-slate-800 h-24 shadow-sm shadow-slate-100 bg-white")
        }else{
            setBg("transparent text-white h-32 ")
        }
    },[clientWindowHeight])

    return (
        <div className={`w-full flex h-30 px-[5%] items-center justify-center z-50 transition duration-700 fixed ${bg}`}>
            <div className='w-full flex justify-between'>
                <div className='w-fit font-rowdie font-[400] text-4xl text-main1'>
                    <h3>Spring</h3>
                </div>
                <div className='xl:w-[55%] lg:w-[70%] sm:w-[70%] hidden lg:flex justify-between items-center flex-row font-manrope'>
                    <div className='flex gap-12 text-main2'>
                        <p onClick={scrollObj.about} aria-hidden="true">About</p>
                        <p onClick={scrollObj.causes} aria-hidden="true">Causes</p>
                        <p onClick={scrollObj.services} aria-hidden="true">Services</p>
                        <p onClick={scrollObj.testimonies} aria-hidden="true">Events</p>
                        <p onClick={scrollObj.about} aria-hidden="true">Gallery</p>
                    </div>
                    <div>
                        <button className='text-main3 font-manrope font-extrabold border border-main3 p-3 rounded-3xl'>CONTACT US</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Navbar
