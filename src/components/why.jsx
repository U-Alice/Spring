import { Fade } from "react-reveal";
import Header from "./header";

export default function Why() {
  return (
    <div className="flex h-fit w-full flex-col gap-12 p-[5%] sm:flex-row lg:p-[10%]">
      <Fade left>
      <div className="flex w-[100%] flex-col items-center justify-center gap-12 sm:w-[60%] md:w-[50%]">
        <Header title={"WHY"} subTitle={"Why Does It Matter"} />
        <p className="mt text-left font-manrope text-xl font-light">
          Education for kids today is the foundation of the nation’s future.
          With Rwanda’s goal to revolutionalize the technology sector, efforts
          need to be made to train the young and equip them with skills required
          to contribute to this sector.
        </p>
        <button className="rounded-3xl border border-main3 p-3 font-manrope font-extrabold text-main3 ">
          Know More
        </button>
      </div>
      </Fade>
      <Fade right>
      <div className="">
        <img src="../../why.png" alt="" />
      </div>
      </Fade>
    </div>
  );
}
