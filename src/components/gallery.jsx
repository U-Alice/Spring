import { Fade } from "react-reveal";
import Header from "./header";

export default function Gallery() {
  return (
    <Fade>
      <div className="mt-32">
        <Header title={"OUR GALLERY"} subTitle={"View Our Gallery"} />
        <div class="container mx-auto px-5 py-2 lg:px-12 lg:pt-24">
          <div class="-m-1 flex flex-wrap md:-m-2">
            <div class="flex w-1/2 flex-wrap">
              <div class="w-1/2 p-1 md:p-2">
                <img
                  alt="gallery"
                  class="block h-full w-full rounded-lg object-cover object-center"
                  src="../../img16.jpg"
                />
              </div>
              <div class="w-1/2 p-1 md:p-2">
                <img
                  alt="gallery"
                  class="block h-full w-full rounded-lg object-cover object-center"
                  src="../../img14.jpg"
                />
              </div>
              <div class="w-full p-1 md:p-2">
                <img
                  alt="gallery"
                  class="block h-full w-full rounded-lg object-cover object-center"
                  src="../../img1.jpg"
                />
              </div>
            </div>
            <div class="flex w-1/2 flex-wrap">
              <div class="w-full p-1 md:p-2">
                <video
                  className="block h-full w-full rounded-lg object-cover object-center"
                  autoPlay
                  loop
                  muted
                >
                  <source
                    src="../../Rwanda-Alice-4.MOV"
                    type="video/mp4"
                  ></source>
                  <track
                    src="captions_en.vtt"
                    kind="captions"
                    srclang="en"
                    label="english_captions"
                  />
                </video>
              </div>
              <div class="w-1/2 p-1 md:p-2">
                <img
                  alt="gallery"
                  class="block h-full w-full rounded-lg object-cover object-center"
                  src="../../img1.jpg"
                />
              </div>
              <div class="w-1/2 p-1 md:p-2">
                <img
                  alt="gallery"
                  class="block h-full w-full rounded-lg object-cover object-center"
                  src="../../img2.jpg"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fade>
  );
}
