/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        'rowdie': 'Rowdies',
        'manrope': 'Manrope'
      },
      colors: {
        'main1': 'rgba(206, 124, 2, 0.5)',
        'main2': 'rgba(58, 23, 0, 1)',
        'main3': 'rgba(58, 23, 0, 0.5)',
        'pink': 'rgba(255, 0, 153, 0.5)'
      },
      backgroundColor: {
        'bmain1': 'rgba(243, 242, 231, 1)',
        'fill': 'linear-gradient(0deg, #F3F2E7 0%, #F3F2E7 100%), rgba(206, 124, 2, 0.50)',
        'footer': 'rgba(255, 0, 153, 0.50)'
      },
      keyframes: {
        animatedgradient: {
          '0%': { backgroundPosition: '0% 50%' },
          '100%': { backgroundPosition: '100% 50%' },
        } 
      },
      animation: {
        moveGradient: 'animatedgradient 6s ease infinite ',
      }
    },
  },
  plugins: [],
}

